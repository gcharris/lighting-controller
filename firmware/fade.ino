#include  "fade.h"

const int redPin_1 = 10;
const int greenPin_1 = 11;
const int bluePin_1 = 9;

const int redPin_2 = 5;
const int greenPin_2 = 6;
const int bluePin_2 = 3;

ColorQueue queue;
int notify = false;
byte bytes[16];

int pos = 0;
/*
Each segment sent is 16 bytes in total. Byte 0 indicates the start of the sequence and its always 0x02.
Byte 1 is the control byte; the LSB is for if looping is on or off, the next LSB is if to empty the queue.
Bytes 2-4 are RGB for channel 1. Bytes 5-7 are RGB for channel 2. Byte 8 is the fade type; currently we
only have two types 0x00 - abrupt change, 0x01 - fade. Bytes 9-10 are the fade length in ms. Bytes 11-14 is 
the solid length in ms. Byte 15 is the last byte and is always 0x03.  
*/
void readInDataFromSerial(){ 
	 while(Serial.available()>0) {
    		digitalWrite(13, HIGH);
		//Keep reading until we get the starting byte
    		if(pos == 0 && Serial.peek()  != 2){
			//Junk data so throw away
			Serial.read();
    		}else if(pos<16){
			byte data = Serial.read();
			bytes[pos++] = data;
		
			if(pos == 16){
				if(data == 3){
					if(isolateEmptyQueueBit(bytes[1])){
                				emptyQueue();
                			}
					
                        		serialNewColor(bytes);
				}
				pos = 0;	
			}
		}
	}		
digitalWrite(13, LOW);
}

//Join two bytes into an unsigned int
unsigned int bytesToUInt(byte first, byte second){
	unsigned int i = 0;
	i = (i << 8) + second;
	i = (i << 8) + first;
    return i;
}

//Join four bytes into an unsigned long
unsigned long bytesToULong(byte first, byte second, byte third, byte forth){
    unsigned long l = 0;
    
	l = (l << 8) + forth;
	l = (l << 8) + third;
	l = (l << 8) + second;
	l = (l << 8) + first;
    return l;
    
}

//Bitmask to extract the empty queue bit from the control byte
bool isolateEmptyQueueBit(byte b){
	return (b & 0x02) >> 1;
}


//void setLooping(bool looping){
//	loop_queue = looping;
//}


void serialNewColor(byte* bytes){
    if(queue.size < 50){
        Color* color = (Color*) malloc(sizeof(Color));
        
        color->next = NULL;
        
        RGB rgb1, rgb2;
        //Channel 1
        rgb1.r =  bytes[2];
        rgb1.g =  bytes[3];
        rgb1.b =  bytes[4];
        
        //Channel 2
        rgb2.r =  bytes[5];
        rgb2.g =  bytes[6];
        rgb2.b =  bytes[7];
        
        color->channel_1 = rgb1;
        color->channel_2 = rgb2;
        
        color->fade_type = bytes[8];
        
        color->fade_step_duration = bytesToUInt(bytes[9], bytes[10]);
        color->solid_duration = bytesToULong(bytes[11], bytes[12], bytes[13], bytes[14]);
        
        addToQueue(color);
        notify = true;
    }
    
}

//Add a new color to the queue of colors to be displayed
void addToQueue(struct Color* new_color){
    
	//If it is null that probably means the ram is full and malloc cannot allocate enough memory
	//We should return a message to the client if this happens
	if(new_color != NULL){
		if(queue.head == NULL && queue.tail == NULL){
			queue.head = queue.tail = new_color;
		}else{
			Color* temp_tail = queue.tail;
			temp_tail->next = new_color;
			queue.tail = new_color;
		}
		queue.size++;
	}
}

//Empties out the queue except for one segment
void emptyQueue(){
	if(queue.size > 0 && queue.head != NULL){
		
		//Get current color
		Color* current = queue.head;

		Color* head = current->next;
		
		while(head != NULL) {
    		Color* temp = head;
    		head = temp->next;
    		free(temp);
    		queue.size--;
		}
		
	    //We are deleting the rest, so make tail = head
		queue.tail = queue.head;
		current->next = NULL;

	}
}

void doNextColor(){
	if(queue.head != NULL){
		Color* current = queue.head;
        
		if(queue.size > 1 && queue.head->next != NULL){
			Color* next = queue.head->next;
            
            queue.head = next;			
			doTrans(current, next);
            
            queue.size--;

				if(queue.size > 1){
					free(current);
				}else if(queue.size == 1){
					free(current);
                    queue.head = next;
                    queue.tail = next;
                    next->next = NULL;
                }

		}else{
			
		if(notify == true){
			Serial.println(1);
			notify = false;
		}
			//Only 1 so display it statically
            updateLED(&current->channel_1, &current->channel_2);


		}
	}
    
}


/*
Own implementation of delay, allows us to receive serial 
color segements and enqueue the during delay time
*/
void fade_delay(unsigned long delay){
    
    unsigned long now, end;
    now = millis();
    end = now + delay;
    
    /*
    Note: I measured the time that readInDataFromSerial took on an atmega328
    and it was always less than 1 ms. So no worries on running it every time
    */
    while(now < end){
        //While waiting do something useful
        readInDataFromSerial();
    	now = millis();
    }
}

void doTrans(Color* current, Color* next){
//Turn on indicator when trans starts
digitalWrite(13, HIGH);	  
    switch(next->fade_type){
        case 0:
            updateLED(&next->channel_1, &next->channel_2);
            break;
            
        case 1:
            colorFade(current, next);
            break;   
    }
    //Turn off indicator when trans is done
    digitalWrite(13,LOW);
    fade_delay(next->solid_duration);
}


//Fade between colors
void colorFade(Color* current, Color* next) {
	
	struct HSV currentHsv_1, nextHsv_1, currentHsv_2, nextHsv_2;
    
	currentHsv_1 = rgb2hsv(&current->channel_1);
	nextHsv_1 = rgb2hsv(&next->channel_1);
	
	currentHsv_2 = rgb2hsv(&current->channel_2);
	nextHsv_2 = rgb2hsv(&next->channel_2);
    
	int currHue_1 = hsvFloatToInt(currentHsv_1.h);
	int currValue_1 = hsvFloatToInt(currentHsv_1.v);
	int currSat_1 = hsvFloatToInt(currentHsv_1.s);
    
	int finalHue_1 = hsvFloatToInt(nextHsv_1.h);
	int finalValue_1 = hsvFloatToInt(nextHsv_1.v);
	int finalSat_1 = hsvFloatToInt(nextHsv_1.s);
	
	int currHue_2 = hsvFloatToInt(currentHsv_2.h);
	int currValue_2 = hsvFloatToInt(currentHsv_2.v);
	int currSat_2 = hsvFloatToInt(currentHsv_2.s);
    
	int finalHue_2 = hsvFloatToInt(nextHsv_2.h);
	int finalValue_2 = hsvFloatToInt(nextHsv_2.v);
	int finalSat_2 = hsvFloatToInt(nextHsv_2.s);
    
	while(currHue_1 != finalHue_1 || currValue_1 != finalValue_1 || currSat_1 != finalSat_1 || currHue_2 != finalHue_2 || currValue_2 != finalValue_2 || currSat_2 != finalSat_2) {
		struct HSV currHsv_1, currHsv_2;
		struct RGB rgb_1, rgb_2;
		
		stepHSV(currHue_1, finalHue_1, currValue_1, finalValue_1, currSat_1, finalSat_1);
		stepHSV(currHue_2, finalHue_2, currValue_2, finalValue_2, currSat_2, finalSat_2);
        
		currHsv_1.h = ((float)currHue_1)/1024;
		currHsv_1.v = ((float)currValue_1)/1024;
		currHsv_1.s = ((float)currSat_1)/1024;
		
		currHsv_2.h = ((float)currHue_2)/1024;
		currHsv_2.v = ((float)currValue_2)/1024;
		currHsv_2.s = ((float)currSat_2)/1024;
        
		rgb_1 = hsv2rgb(currHsv_1);
		rgb_2 = hsv2rgb(currHsv_2);
		
        updateLED(&rgb_1, &rgb_2);
        
		fade_delay(next->fade_step_duration);
	}
}

int hsvFloatToInt(float value){
	return (int) (value * 1024);
}

//Determine the shortest hue shift (forward or backward) between two hue values
bool forwardBackwardHue(int currHue, int finalHue){
	//Calculate forward and backward hue distances
	int forward, backward;
    
	if(currHue <= finalHue){
		forward =  finalHue - currHue;
		backward = 1024 - finalHue + currHue;
	}else{
		forward = 1024 - currHue + finalHue;
		backward = currHue - finalHue;
	}
    
	if(forward <= backward){
		return true;
	}else{
		return false;
	}
}

void stepHSV(int& currHue, int& finalHue, int& currValue, int finalValue, int& currSat, int& finalSat){
	
    /*  This case is for when either the final or current color value is o,o,o (off)
     If either case is off then when transitioning the hue and sat will drop to nothing
     during the transition. To deal with this, in the case of a 0,0,0 we set the hue and 
     sat to the respective final or curr value
     */
    
    if(currValue == 0 && currHue == 0 && currSat == 0){
        currHue = finalHue;
        currSat = finalSat;
        
    }else if(finalSat == 0 && finalHue == 0 && finalValue == 0){
        finalHue = currHue;
        finalSat = currSat;
    }
    
    if(currHue != finalHue){
    	//Determine the shortest distance between the hues of the two colors
        if(forwardBackwardHue(currHue, finalHue)){
            if(currHue == 1023){
                currHue = 0;
            }else{
                currHue++;
            }
        }else{
            if(currHue == 0){
                currHue = 1023;
            }else{
                currHue--;
            }
        }
    }
    if(currValue<finalValue){
        currValue++;
    }
    else if(currValue>finalValue){
        currValue--;
    }
    
    if(currSat<finalSat){
        currSat++;
    }
    else if(currSat>finalSat){
        currSat--;
    }
}

//Given struct RGB's, update the PWM for the two channels
void updateLED(RGB* channel_1, RGB* channel_2){
    analogWrite(redPin_1, channel_1->r);
    analogWrite(greenPin_1, channel_1->g);
    analogWrite(bluePin_1, channel_1->b);
    analogWrite(redPin_2, channel_2->r);
    analogWrite(greenPin_2, channel_2->g);
    analogWrite(bluePin_2, channel_2->b);
}


void setup() {
	pinMode(redPin_1, OUTPUT);
	pinMode(greenPin_1, OUTPUT);
	pinMode(bluePin_1, OUTPUT);
	pinMode(redPin_2, OUTPUT);
	pinMode(greenPin_2, OUTPUT);
	pinMode(bluePin_2, OUTPUT);
	pinMode(13, OUTPUT);
	Serial.begin(9600);
	
	    Color* color = (Color*) malloc(sizeof(Color));     
        color->next = NULL;        
        
        RGB rgb;
        rgb.r = 0;
        rgb.g = 0;
        rgb.b = 0;       
        color->channel_1 = rgb;
        color->channel_2 = rgb;
        color->fade_type = 0;
        color->fade_step_duration = 0;
        color->solid_duration = 0;
        addToQueue(color);
}

void loop() {
    readInDataFromSerial();
	doNextColor();
}

/*
Color conversion
*/
struct HSV rgb2hsv(struct RGB* color)
{
	struct HSV out;
	float vmin, vmax, delta, rc, gc, bc;
    
	//Convert RGB 0-255 to 1.0 floats
	rc = (float)color->r / 255.0;
	gc = (float)color->g / 255.0;
	bc = (float)color->b / 255.0;
    
	//Determine the current maximum color value RGB
	vmax = MAX(rc, MAX(gc, bc));
    
	//Determine the current minimum color value RGB
	vmin = MIN(rc, MIN(gc, bc));
    
	//Compare the difference between min and max
	delta = vmax - vmin;
    
	//Difference between min and max equals color value
	out.v = vmax;
    
	if (vmax != 0.0){
		out.s = delta / vmax;
	}
	else{
		out.s = 0.0;
	}
    
	if (out.s == 0.0) {
		out.h = 0.0;
	}
	else {
		if (rc == vmax){
			out.h = (gc - bc) / delta;
		}
		else if (gc == vmax){
			out.h = 2 + (bc - rc) / delta;
		}
		else if (bc == vmax){
			out.h = 4 + (rc - gc) / delta;
		}
		out.h /= 6.0;
		if (out.h < 0){
			out.h += 1.0;
		}
	}
	return out;
}

struct RGB hsv2rgb(struct HSV hsv){
	// HSV scaled to to the color wheel (0-255)
    
	float h, s, v;
	struct RGB rgb;
    
	float r = 0;
	float g = 0;
	float b = 0;
    
	// Scale Hue to be between 0 and 360. Saturation
	// and value scale to be between 0 and 1.
	// We mod it with 360 so the value is never over 1.0 (if by accident)
	h = (float) fmod((hsv.h * 360),360);
    
	s = (float) hsv.s;
	v = (float) hsv.v;
    
	if ( s == 0 ) {
		// If s is 0, all colors are the same.
		// This is some flavor of gray.
		r = v;
		g = v;
		b = v;
        
	}else{
        
		float p, q, t;
		float fractionalSector, sectorPos;
		int sectorNumber;
        
		// The color wheel has 6 sectors.
		// Which sector you're in.
		sectorPos = h / 60;
		sectorNumber = (int)(floor(sectorPos));
        
		// get the fractional part of the sector.
		// ie how many degrees into the sector
		fractionalSector = sectorPos - sectorNumber;
        
		// Calculate values for the three axes
		// of the color.
		p = v * (1 - s);
		q = v * (1 - (s * fractionalSector));
		t = v * (1 - (s * (1 - fractionalSector)));
        
		// Assign the fractional colors to r, g, and b
		// based on the sector the angle is in.
		switch (sectorNumber) {
            case 0:
                r = v;
                g = t;
                b = p;
                break;
                
            case 1:
                r = q;
                g = v;
                b = p;
                break;
                
            case 2:
                r = p;
                g = v;
                b = t;
                break;
                
            case 3:
                r = p;
                g = q;
                b = v;
                break;
                
            case 4:
                r = t;
                g = p;
                b = v;
                break;
                
            case 5:
                r = v;
                g = p;
                b = q;
                break;
		}
	}
	//Convert to 0-255 int form
	rgb.r = (int)(r * 255);
	rgb.g = (int)(g * 255);
	rgb.b = (int)(b * 255);
	return rgb;
}

