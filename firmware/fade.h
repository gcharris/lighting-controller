#define true 1
#define false 0
#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

//HSV 0-1.0 for all variables
struct HSV {
	float h;       // hue
	float s;       // saturation
	float v;       // brightness
};

//RGB 0-255 for all variables
struct RGB{
	uint8_t r;           // Red
	uint8_t g;           // Green
	uint8_t b;           // Blue
};

struct Color{
	struct Color* next;
	struct RGB channel_1;
	struct RGB channel_2;
	uint8_t fade_type;
	unsigned int fade_step_duration;
	unsigned long solid_duration;
    
};

struct ColorQueue{
	struct Color* head;
	struct Color* tail;
	unsigned int size;
};

struct HSV rgb2hsv(struct RGB* color);
struct RGB hsv2rgb(struct HSV hsv);
void addToQueue(struct Color* new_color);
bool forwardBackwardHue(int currHue, int finalHue);
void updateLED(RGB* channel_1, RGB* channel_2);
unsigned int bytesToUInt(byte first, byte second);
unsigned long bytesToULong(byte first, byte second, byte third, byte forth);
void serialNewColor(byte* bytes);
void setLooping(bool looping);
void emptyQueue();
int isolateLoopingBit(byte b);
bool isolateEmptyQueueBit(byte b);
void readInDataFromSerial(); 
void fade_delay(unsigned long delay);
void stepHSV(int& currHue, int& finalHue, int& currValue, int finalValue, int& currSat, int& finalSat);
int hsvFloatToInt(float value);
void colorFade(Color* current, Color* next);
void doTrans(Color* current, Color* next);
void doNextColor();