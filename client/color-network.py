#! /usr/bin/env python
import json
import time 
import struct
import threading, Queue
import termios, fcntl, sys, os
import socket
from prettytable import *

class SocketListenThread(threading.Thread):

    def __init__(self, sock, sequences, result_q):
        super(SocketListenThread, self).__init__()
        self.result_q = result_q
        self.sequences = sequences
        self.stoprequest = threading.Event()
        self.sock = sock

    def run(self):
        while not self.stoprequest.isSet():
            try:
                result = self.sock.recv(4096)
		print result
                self.sequences.put(result)
            except Queue.Empty:
                continue
	
    def join(self, timeout=None):
        self.stoprequest.set()
        super(SocketListenThread, self).join(timeout)
        
        
class SocketThread(threading.Thread):

    def __init__(self, sock, sequences, result_q):
        super(SocketThread, self).__init__()
        self.sequences = sequences
        self.result_q = result_q
        self.stoprequest = threading.Event()
        self.sock = sock

    def run(self):
        while not self.stoprequest.isSet():
            try:
            	# Grab the next sequence dictionary off the queue
                sequence = self.sequences.get(True, 0.05)
                self.sendSegements(sequence)
                if (len(sequence['segments']) > 1):
                	self.result_q.put(sequence)
            except Queue.Empty:
                continue
                
    def sendSegements(self,sequence):
        # Set first to 1, so when a new sequence plays it clears out the last
        first = 1
        for item in sequence['segments']:
	    tup1 = self.hex_to_rgb(item['hex_1'])
	    tup2 = self.hex_to_rgb(item['hex_2'])
	    conf = self.makeConfByte(first, 1)			
	    
	    # begin byte
	    self.sock.send(chr(2))
	    # conf byte
	    self.sock.send(chr(conf))
			
	    # RGB for the two channels
	    self.sock.send(chr(tup1[0]))
	    self.sock.send(chr(tup1[1]))
	    self.sock.send(chr(tup1[2]))
	    self.sock.send(chr(tup2[0]))
	    self.sock.send(chr(tup2[1]))
	    self.sock.send(chr(tup1[2]))
		
	    # transition type - 0 none, 1 fade		
	    self.sock.send(chr(item['fade_type']))
 		
	    # transition step, two bytes
	    self.sock.send(self.packIntegerAsUInt(item['fade_step_duration_ms'])) 
		
	    # solid length of time, 4 bytes
	    self.sock.send(self.packIntegerAsULong(item['solid_duration_ms']))  
		
	    # end byte
	    self.sock.send(chr(3))
	    first = 0
	
    def join(self, timeout=None):
        self.stoprequest.set()
        super(SocketThread, self).join(timeout)
        
    def packIntegerAsULong(self, value):
    	return struct.pack('I', value) #should check bounds

    def packIntegerAsUInt(self, value):
    	return struct.pack('H', value)

    def hex_to_rgb(self, value):
    	value = value.lstrip('#')
    	lv = len(value)
    	return tuple(int(value[i:i+lv/3], 16) for i in range(0, lv, lv/3))
    	
    def makeConfByte(self, empty, loop):
    	return ((empty & 1) << 1) | (loop & 1)
    
def printSequenceList(seqs):

    table = PrettyTable(["Key", "Name"])
    
    for seq in seqs:
	    table.add_row([seq['hotkey'], seq['name']])
	
    table.align["key"] = "c"
    table.align["Name"] = "l"
    print table
	                
def main(args):

    HOST, PORT = args[0], 2000
    seqFile = "sequences.json"
	
    fd = sys.stdin.fileno()

    oldterm = termios.tcgetattr(fd)
    newattr = termios.tcgetattr(fd)
    newattr[3] = newattr[3] & ~termios.ICANON & ~termios.ECHO
    termios.tcsetattr(fd, termios.TCSANOW, newattr)

    oldflags = fcntl.fcntl(fd, fcntl.F_GETFL)
    fcntl.fcntl(fd, fcntl.F_SETFL, oldflags | os.O_NONBLOCK)

    # Create a single input and a single output queue for all threads.
    sequences = Queue.Queue()
    result_q = Queue.Queue()
    
    json_data = open(seqFile)
    seqs = json.load(json_data)
    
    # SOCK_STREAM == a TCP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((HOST, PORT))

    # Create the "thread pool"

    thread = SocketThread(sock = sock, sequences=sequences, result_q=result_q)
    thread.start()
    	
   # threadListen = SocketListenThread(sock = sock, sequences=sequences, result_q=result_q)
   # threadListen.start()


    os.system('clear')
    printSequenceList(seqs)
    print ("Connected to: " +  str(args[0]))
    print "Press ESC to disconnect"
    print "Hit key to play sequence"
    print
	
    try:
        while 1:
            try:
                c = sys.stdin.read(1)
                for seq in seqs:
			if(c == chr(27)):
				sock.close()
				sys.exit(0)
			elif(seq['hotkey'] == c):
                		with result_q.mutex:
    						result_q.queue.clear()
                		sequences.put(seq)
            except IOError: pass
    finally:
        termios.tcsetattr(fd, termios.TCSAFLUSH, oldterm)
        fcntl.fcntl(fd, fcntl.F_SETFL, oldflags)
    thread.join()
   # threadListen.join()
if __name__ == '__main__':
    import sys
    main(sys.argv[1:])
